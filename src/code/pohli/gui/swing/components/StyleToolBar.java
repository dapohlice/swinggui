package code.pohli.gui.swing.components;

import javax.swing.JToolBar;

import code.pohli.gui.swing.style.StyleColor;


public class StyleToolBar extends JToolBar {
	
	public StyleToolBar(){
		super();
		
		this.setBackground(StyleColor.Color.getDarkB());
		this.setBorderPainted(false);
		
	}
}
