package code.pohli.gui.swing.components;

import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

public class StyleImageLabel extends StyleLabel {
	
	private int scale;
	
	public StyleImageLabel(int scale){
		super();
		this.scale = scale;
	}
	
	public StyleImageLabel(int scale,Image img){
		this(scale);
		this.setImage(img);
	}
	
	public void setImage(Image img)
	{
		img = img.getScaledInstance(scale, scale,  java.awt.Image.SCALE_SMOOTH);
		ImageIcon ico = new ImageIcon(img);
		
		this.setIcon(ico);

	}
}
