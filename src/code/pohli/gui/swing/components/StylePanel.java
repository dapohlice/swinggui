package code.pohli.gui.swing.components;

import javax.swing.JPanel;

import code.pohli.gui.swing.style.StyleColor;



public class StylePanel extends JPanel {
	public StylePanel(){
		super();
		
		this.setBackground(StyleColor.Color.getDarkB());
	}
}
