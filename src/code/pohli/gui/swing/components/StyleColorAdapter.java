package code.pohli.gui.swing.components;

import java.awt.Color;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public class StyleColorAdapter extends XmlAdapter<Integer, Color>
{

	public StyleColorAdapter() {
		super();
	}
	
	@Override
	public Integer marshal(Color v) throws Exception {
		return v.getRGB();
		
	}

	@Override
	public Color unmarshal(Integer v) throws Exception {
		
		return new Color(v);
	}


	
}