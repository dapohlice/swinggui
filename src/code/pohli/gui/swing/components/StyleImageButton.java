package code.pohli.gui.swing.components;

import java.awt.Dimension;
import java.awt.Image;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

public class StyleImageButton extends StyleButton{
	
	private int scale;
	private String text;
	
	@Deprecated
	public StyleImageButton(String text,String image,int scale){
		super();
		this.scale = scale;
		this.setImageScaled(image);
	}
	
	public StyleImageButton(String text,Image image,int scale){
		super();
		this.scale = scale;
		this.setImage(image);
	}
	
	public StyleImageButton(String text,int scale){
		super();
		this.scale = scale;
	}
	
	@Deprecated
	private void setImageScaled(String image)
	{
		try {
			Image img = ImageIO.read(new File(image));
			img = img.getScaledInstance(scale, scale,  java.awt.Image.SCALE_SMOOTH);
			ImageIcon ico = new ImageIcon(img);
			
			this.setIcon(ico);
		} catch (IOException e) {
			this.setText(text);
			e.printStackTrace();
		}
	}
	

	public void setImage(Image img)
	{
		img = img.getScaledInstance(scale, scale,  java.awt.Image.SCALE_SMOOTH);
		ImageIcon ico = new ImageIcon(img);
		this.setIcon(ico);
		this.setPreferredSize(new Dimension(scale, scale));
		

	}
}
