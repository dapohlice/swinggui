package code.pohli.gui.swing.components;

import java.awt.Insets;
import java.awt.event.MouseEvent;

import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.border.EmptyBorder;

import code.pohli.gui.swing.style.StyleColor;



public class StyleButton extends JButton {
	
	public StyleButton()
	{
		super();
		
		this.setMargin(new Insets(0, 0, 10, 15));
		
		//this.setPreferredSize(new Dimension(100, 200));
		
		this.setBackground(StyleColor.Color.getDarkA());
		this.setForeground(StyleColor.Color.getLightA());
		
		this.setBorder(new EmptyBorder(10, 10, 10, 10));
		
		this.setBorderPainted(false);
		
		this.addMouseListener(new java.awt.event.MouseAdapter() {
		    public void mouseEntered(java.awt.event.MouseEvent evt) {
		        evt.getComponent().setBackground(StyleColor.Color.getLightC());
		    }
		    
		    public void mousePressed(MouseEvent evt)
		    {
		    	evt.getComponent().setForeground(StyleColor.Color.getDarkA());
		    }
		    public void mouseReleased(MouseEvent evt)
		    {
		    	evt.getComponent().setForeground(StyleColor.Color.getLightA());
		    }
		    
		    public void mouseExited(java.awt.event.MouseEvent evt) {
		        evt.getComponent().setBackground(StyleColor.Color.getDarkA());
		        evt.getComponent().setForeground(StyleColor.Color.getLightA());
		    }
		});
	}
	
	
	public StyleButton(String text){
		this();
		this.setText(text);
	}
	
	public StyleButton(Icon icon){
		this();
		this.setIcon(icon);
	}
	

	
}
