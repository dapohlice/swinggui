package code.pohli.gui.swing.components;

import java.awt.Image;
import java.nio.file.Path;

public interface IStyleListElement {

	public Image getImage();
	public void open();
	public void click();
	public Path getPath();
}
