package code.pohli.gui.swing.components;

import javax.swing.JLabel;

import code.pohli.gui.swing.style.StyleColor;



public class StyleLabel extends JLabel implements IStyle{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6794070546918016747L;

	public StyleLabel(String text) {
		super(text);
		this.setStyle();
	}

	public StyleLabel(){
		super();
		setStyle();
		
	}

	@Override
	public void setStyle() {
		this.setForeground(StyleColor.Color.getLightA());
		this.setBackground(StyleColor.Color.getDarkB());
		
	}
	
	
}
