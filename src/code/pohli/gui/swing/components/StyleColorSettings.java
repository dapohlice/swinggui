package code.pohli.gui.swing.components;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JPanel;

import code.pohli.gui.swing.style.StyleColor;



public class StyleColorSettings extends StylePanel{

	private class ColorPanel extends StylePanel{
		
		private Color color;
		private JButton btn;
		
		public ColorPanel(String name, Color color)
		{
			LayoutManager lm = new FlowLayout();/*new BoxLayout(this,BoxLayout.X_AXIS);*/
			this.setLayout(lm);
			
			this.add(new StyleLabel(name));
			btn = new JButton();
			btn.setBackground(color);
			btn.addActionListener(new ColorBtnListener(this));
			this.add(btn);
			this.color = color;
		}
		
		public void setColor(Color c)
		{
			this.color = c;
			btn.setBackground(c);
		}
		
	}
	
	private class ColorBtnListener implements ActionListener{

		private ColorPanel c;
		
		public ColorBtnListener(ColorPanel c) {
			this.c = c;
		}
		
		@Override
		public void actionPerformed(ActionEvent arg0) {
			 this.c.setColor(JColorChooser.showDialog(null,"Farbauswahl", null));
			
		}
	}
	
	
	
	private class SaveSettingsListener implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent arg0) {
			if(StyleColor.save()){
				System.out.println("saved");
			}else{
				System.out.println("failed");
			}
		}
	}
	
	
	private class AbortSettingsListener implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent arg0) {
			System.out.println("abort");		
		}
	}
	
	private class ResetSettingsListener implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent arg0) {
			StyleColor.Color = new StyleColor();
			StyleColor.save();
		}
	}
	
	
	
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3028830828999184133L;

	public StyleColorSettings() {
		
		LayoutManager lm = new BoxLayout( this, javax.swing.BoxLayout.Y_AXIS);
		this.setLayout(lm);
		
		
		this.add(new ColorPanel("Dark A",StyleColor.Color.getDarkA()));
		
		
		this.add(new ColorPanel("Dark B",StyleColor.Color.getDarkB()));
		this.add(new ColorPanel("Light A",StyleColor.Color.getLightA()));
		this.add(new ColorPanel("Light B",StyleColor.Color.getLightB()));
		this.add(new ColorPanel("Light C",StyleColor.Color.getLightC()));
		
		JPanel p = new StylePanel();
		p.setLayout(new FlowLayout());
		
		StyleButton btn = new StyleButton("save");
		btn.addActionListener(new SaveSettingsListener());
		p.add(btn);
		
		btn = new StyleButton("abort");
		btn.addActionListener(new AbortSettingsListener());
		p.add(btn);
		
		btn = new StyleButton("reset");
		btn.addActionListener(new ResetSettingsListener());
		p.add(btn);
		
		this.add(p);

	}

	
	

}
