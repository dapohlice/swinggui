package code.pohli.gui.swing.components;

import java.util.Vector;

import javax.swing.ComboBoxModel;
import javax.swing.JComboBox;

import code.pohli.gui.swing.style.StyleColor;



public class StyleComboBox<E> extends JComboBox<E> implements IStyle {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5952152829764656472L;



	
	
	public StyleComboBox() {
		super();
		this.setStyle();
	}





	public StyleComboBox(ComboBoxModel<E> aModel) {
		super(aModel);
		this.setStyle();
	}





	public StyleComboBox(E[] items) {
		super(items);
		this.setStyle();
	}





	public StyleComboBox(Vector<E> items) {
		super(items);
		this.setStyle();
	}





	public void setStyle(){
		this.setForeground(StyleColor.Color.getDarkA());
		this.setBackground(StyleColor.Color.getLightA());
	}
	
}
