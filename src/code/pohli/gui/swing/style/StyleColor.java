package code.pohli.gui.swing.style;

import java.awt.Color;
import java.io.File;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

@XmlRootElement(name = "styleColor")
public class StyleColor {
	
		
	public static StyleColor Color = new StyleColor();
	
	public static boolean initialse(){
		
		try{
			JAXBContext context = JAXBContext.newInstance(StyleColor.class);
			Unmarshaller um = context.createUnmarshaller();
			Color = (StyleColor)um.unmarshal(new File("settings/color.xml"));
		} catch (JAXBException e) {
			e.printStackTrace();
			Color = new StyleColor();	
			return false;
		}
		return true;

		
	}
	
	public static boolean save(){
		
		File dir = new File("settings");
		if(!dir.exists())
		{
			dir.mkdir();
		}
		
		try {
			JAXBContext context = JAXBContext.newInstance(StyleColor.class);
			Marshaller m = context.createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT,Boolean.TRUE);
			m.marshal(Color, new File("settings/color.xml"));
		} catch (JAXBException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	private Color lightA = new Color(169, 226, 249);
	private Color lightB = new Color(98, 122, 132);
	private Color lightC = new Color(100, 123, 132);
	
	private Color darkA = new Color(65, 82, 89);
	private Color darkB = new Color(54, 105, 127);
	
	private Color highLightA = new Color(255, 166, 109);
	
	public StyleColor(){
		
	}
	
	@XmlJavaTypeAdapter(StyleColorAdapter.class)
	public Color getLightA() {
		return lightA;
	}

		
	public void setLightA(Color lightA) {
		this.lightA = lightA;
	}

	@XmlJavaTypeAdapter(StyleColorAdapter.class)
	public Color getLightB() {
		return lightB;
	}

	public void setLightB(Color lightB) {
		this.lightB = lightB;
	}

	@XmlJavaTypeAdapter(StyleColorAdapter.class)
	public Color getLightC() {
		return lightC;
	}

	public void setLightC(Color lightC) {
		this.lightC = lightC;
	}

	@XmlJavaTypeAdapter(StyleColorAdapter.class)
	public Color getDarkA() {
		return darkA;
	}

	public void setDarkA(Color darkA) {
		this.darkA = darkA;
	}
	
	@XmlJavaTypeAdapter(StyleColorAdapter.class)
	public Color getDarkB() {
		return darkB;
	}

	public void setDarkB(Color darkB) {
		this.darkB = darkB;
	}
	
	
}
