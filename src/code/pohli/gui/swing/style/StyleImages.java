package code.pohli.gui.swing.style;

import java.awt.Image;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;


public class StyleImages {

	public static Image loadImage(Path path)
	{
		File f = path.toFile();
		try {
			return ImageIO.read(f);
		}
		catch(Exception e){
	      
			
			System.err.println(f.getAbsolutePath());
			e.printStackTrace();
			return null;
		}
	}
	
	
	public static Image loadImage(String path)
	{
		
		File f = new File(path);
		try {
			return ImageIO.read(f);
		}
		catch(Exception e){
			
			/*try {
				String current;
				current = new java.io.File( "." ).getCanonicalPath();
				System.err.println("Current dir:"+current);
				String currentDir = System.getProperty("user.dir");
				System.err.println("Current dir using System:" +currentDir);
			} catch (IOException e1) {
				System.err.println("Failed pwd");
			}*/
	       
			
			System.err.println(f.getAbsolutePath());
			e.printStackTrace();
			return null;
		}
	}
	
	public static ImageIcon genIcon(Image img){
		return genIcon(img,16);
	}
	
	
	public static ImageIcon genIcon(Image img,int scale)
	{
		img = img.getScaledInstance(scale, scale,  java.awt.Image.SCALE_SMOOTH);
		return new ImageIcon(img);
	}
	
}
