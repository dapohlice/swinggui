package code.pohli.gui.swing.style;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JPanel;

import code.pohli.gui.swing.components.StyleButton;
import code.pohli.gui.swing.components.StyleLabel;
import code.pohli.gui.swing.components.StylePanel;


public class StyleColorSettings extends StylePanel{

	private interface ChangeColor{
		public void changeColor(Color c);
	}
	
	
	private class ColorPanel extends StylePanel{
		
		private Color color;
		private JButton btn;
		private ChangeColor colorChanger;
		
		public ColorPanel(String name, Color color,ChangeColor colorChanger)
		{
			LayoutManager lm = new FlowLayout();/*new BoxLayout(this,BoxLayout.X_AXIS);*/
			this.setLayout(lm);
			
			this.add(new StyleLabel(name));
			btn = new JButton();
			btn.setBackground(color);
			btn.addActionListener(new ColorBtnListener(this));
			this.add(btn);
			this.color = color;
			this.colorChanger = colorChanger;
		}
		
		public void setColor(Color c)
		{
			this.color = c;
			btn.setBackground(c);
			this.colorChanger.changeColor(c);
		}
		
	}
	
	private class ColorBtnListener implements ActionListener{

		private ColorPanel c;
		
		public ColorBtnListener(ColorPanel c) {
			this.c = c;
		}
		
		@Override
		public void actionPerformed(ActionEvent arg0) {
			 this.c.setColor(JColorChooser.showDialog(null,"Farbauswahl", this.c.color));
			
		}
	}
	
	
	
	private class SaveSettingsListener implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent arg0) {
			if(StyleColor.save()){
				System.out.println("saved");
			}else{
				System.out.println("failed");
			}
		}
	}
	
	
	private class AbortSettingsListener implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent arg0) {
			System.out.println("abort");		
		}
	}
	
	private class ResetSettingsListener implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent arg0) {
			StyleColor.Color = new StyleColor();
			StyleColor.save();
		}
	}
	
	
	
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3028830828999184133L;
	
	private void drawColorPanels() {
		
		JPanel colorsPanel = new StylePanel();
		LayoutManager lm = new BoxLayout( colorsPanel, javax.swing.BoxLayout.Y_AXIS);
		colorsPanel.setLayout(lm);

		
		colorsPanel.add(new ColorPanel("Dark A",StyleColor.Color.getDarkA(),new ChangeColor() {
			
			@Override
			public void changeColor(Color c) {
				
				StyleColor.Color.setDarkA(c);
			}
		}));
		
		
		colorsPanel.add(new ColorPanel("Dark B",StyleColor.Color.getDarkB(),new ChangeColor() {
			
			@Override
			public void changeColor(Color c) {
				
				StyleColor.Color.setDarkB(c);
			}
		}));
		colorsPanel.add(new ColorPanel("Light A",StyleColor.Color.getLightA(),new ChangeColor() {
			
			@Override
			public void changeColor(Color c) {
				
				StyleColor.Color.setLightA(c);
			}
		}));
		colorsPanel.add(new ColorPanel("Light B",StyleColor.Color.getLightB(),new ChangeColor() {
			
			@Override
			public void changeColor(Color c) {
				
				StyleColor.Color.setLightB(c);
			}
		}));
		colorsPanel.add(new ColorPanel("Light C",StyleColor.Color.getLightC(),new ChangeColor() {
			
			@Override
			public void changeColor(Color c) {
				
				StyleColor.Color.setLightC(c);
			}
		}));
		this.add(colorsPanel,BorderLayout.CENTER);
	}
	
	private void drawButtons() {
		JPanel p = new StylePanel();
		p.setLayout(new FlowLayout());
		
		StyleButton btn = new StyleButton("save");
		btn.addActionListener(new SaveSettingsListener());
		p.add(btn);
		
		btn = new StyleButton("abort");
		btn.addActionListener(new AbortSettingsListener());
		p.add(btn);
		
		btn = new StyleButton("reset");
		btn.addActionListener(new ResetSettingsListener());
		p.add(btn);
		
		this.add(p,BorderLayout.SOUTH);
	}
	
	private void drawHeader() {
		StyleLabel header = new StyleLabel("Color Settings");
		
		this.add(header,BorderLayout.NORTH);
	}
	
	
	private void drawAll()
	{
		drawHeader();
		drawButtons();
		drawColorPanels();
	}
	
	public StyleColorSettings() {
		
		this.setLayout(new BorderLayout());
		drawAll();
		
		
		
		
		
		

	}
	
	

}
